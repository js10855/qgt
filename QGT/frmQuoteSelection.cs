﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace QGT
{
    public partial class frmQuoteSelection : Form
    {
        string path = Path.GetFullPath(@"Quote List\Quote List.xlsx");
        public frmQuoteSelection()
        {
            InitializeComponent();                     
        }

        private void frmQuoteSelection_Load(object sender, EventArgs e)
        {
            BindQuoteData("");
            BindFilterData();  
            SetParentParameter();
            ResetData();
        }

        public string QuoteNo { get; set; }
        public string ParentName { get; set; }

        public string CustomerName { get; set; }

        public DateTime QuoteDate { get; set; }

        public string PrimaryContact { get; set; }

        private void SetParentParameter()
        {
            if (ParentName == "Main")
            {
                dgvQuotes.Columns["Select"].Visible = false;
                btnCopy.Text = "Reset";                
            }
        }

        private void ResetData()
        {
            cmbCustomer.Text = "";
            cmbQuoteNo.Text = "";
            cmbOperator.Text = "";
            dtFromDate.Checked = false;
            dtToDate.Checked = false;
            txtTotal.Text = "";
        }
        private void BindFilterData()
        {            
            string query = "SELECT QuoteID, QuoteNo FROM [Sheet1$] order by [QuoteNo]";
            cmbQuoteNo.DataSource = DataAccessManager.FetchData(path, query);
            cmbQuoteNo.ValueMember = "QuoteID";
            cmbQuoteNo.DisplayMember = "QuoteNo";  

            string path1 = Path.GetFullPath(@"Master Data\Customer.xlsx");
            query = "SELECT CustomerID, CustomerName FROM [Sheet1$] order by CustomerName";
            cmbCustomer.DataSource = DataAccessManager.FetchData(path1, query);
            cmbCustomer.ValueMember = "CustomerID";
            cmbCustomer.DisplayMember = "CustomerName";
        }
        private void BindQuoteData(string query="")
        {            
            if (query =="")
                query = "SELECT QuoteID,QuoteNo, [Date],Customer,TotalAmount,PrimaryContact FROM [Sheet1$] order by [date] asc";
                        
            dgvQuotes.DataSource = DataAccessManager.FetchData(path, query);
            dgvQuotes.Columns["QuoteID"].Visible = false;
        }   

        private void dgvQuotes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            if (e.ColumnIndex == 0)
             { 
                 for(int i=0;i< dgvQuotes.RowCount ;i++)
                 {
                     if (e.RowIndex != i) 
                     {                        
                         dgvQuotes[e.ColumnIndex, i].Value = Properties.Resources.RadioButtonUnsel;
                         dgvQuotes[e.ColumnIndex, i].Tag = 0;
                     }
                     else
                     {                      
                         dgvQuotes[e.ColumnIndex, i].Value = Properties.Resources.RadioButtonSel;
                         dgvQuotes[e.ColumnIndex, i].Tag = 1;
                     }
                }
             } 
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (btnCopy.Text =="Reset")
            {
                ResetData();
            }
            else
            { 
                for(int i=0;i< dgvQuotes.RowCount ;i++)
                {
                    if ((int) dgvQuotes[0, i].Tag ==1)
                    {
                        QuoteNo = dgvQuotes[2, i].Value.ToString();
                        QuoteDate = Convert.ToDateTime(dgvQuotes[3, i].Value);
                        CustomerName = dgvQuotes[4, i].Value.ToString();
                        PrimaryContact = dgvQuotes[6, i].Value.ToString();
                        this.Close();
                    }
                }
            }
        }

       

        private void btnFilter_Click(object sender, EventArgs e)
        {
            StringBuilder strQuery = new StringBuilder();
            bool prFilter =false;
            strQuery.Append("SELECT QuoteID,QuoteNo, [Date],Customer,TotalAmount,PrimaryContact FROM [Sheet1$] ");

            if (!string.IsNullOrEmpty(cmbCustomer.Text))
            { strQuery.Append(" where Customer='" + cmbCustomer.Text.Trim() + "'"); prFilter = true; }

            if (!string.IsNullOrEmpty(cmbQuoteNo.Text))
            { 
                if (prFilter ==true)
                    strQuery.Append(" and QuoteNo='" + cmbQuoteNo.Text.Trim() + "'");
                else
                { strQuery.Append(" where QuoteNo='" + cmbQuoteNo.Text.Trim() + "'"); prFilter = true;}
            }

            if (dtFromDate.Checked ==true)
            {
                if (prFilter == true)
                    strQuery.Append(" and [Date]>=#" + dtFromDate.Value + "#");
                else
                    { strQuery.Append("  where [Date]>=#" + dtFromDate.Value + "#"); prFilter = true; }
            }

            if (dtToDate.Checked == true)
            {
                if (prFilter == true)
                    strQuery.Append(" and [Date]<=#" + dtToDate.Value + "#");
                else
                    { strQuery.Append(" where [Date]<=#" + dtToDate.Value + "#"); prFilter = true; }
            }

            if ((!string.IsNullOrEmpty(txtTotal.Text.Trim())) && (!string.IsNullOrEmpty(cmbOperator.Text)))
            {
                if (prFilter == true)
                    strQuery.Append(" and TotalAmount " + cmbOperator.Text + "'" + txtTotal.Text + "'" );
                else
                { strQuery.Append(" where TotalAmount " + cmbOperator.Text + "'" + txtTotal.Text + "'"); prFilter = true; }
            }
            
            strQuery.Append(" order by [date] asc");
            BindQuoteData(strQuery.ToString());
        }

        
    }
}
