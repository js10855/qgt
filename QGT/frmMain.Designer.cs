﻿namespace QGT
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mastersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rateMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateQuoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateQuoteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.searchQuoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemFunctionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mastersToolStripMenuItem,
            this.generateQuoteToolStripMenuItem,
            this.systemFunctionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mastersToolStripMenuItem
            // 
            this.mastersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyMasterToolStripMenuItem,
            this.customerMasterToolStripMenuItem,
            this.rateMasterToolStripMenuItem});
            this.mastersToolStripMenuItem.Name = "mastersToolStripMenuItem";
            this.mastersToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.mastersToolStripMenuItem.Text = "Masters";
            // 
            // companyMasterToolStripMenuItem
            // 
            this.companyMasterToolStripMenuItem.Name = "companyMasterToolStripMenuItem";
            this.companyMasterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.companyMasterToolStripMenuItem.Text = "Company Master";
            this.companyMasterToolStripMenuItem.Click += new System.EventHandler(this.companyMasterToolStripMenuItem_Click);
            // 
            // customerMasterToolStripMenuItem
            // 
            this.customerMasterToolStripMenuItem.Name = "customerMasterToolStripMenuItem";
            this.customerMasterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.customerMasterToolStripMenuItem.Text = "Customer Master";
            this.customerMasterToolStripMenuItem.Click += new System.EventHandler(this.customerMasterToolStripMenuItem_Click);
            // 
            // rateMasterToolStripMenuItem
            // 
            this.rateMasterToolStripMenuItem.Name = "rateMasterToolStripMenuItem";
            this.rateMasterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.rateMasterToolStripMenuItem.Text = "Rate Master";
            this.rateMasterToolStripMenuItem.Click += new System.EventHandler(this.rateMasterToolStripMenuItem_Click);
            // 
            // generateQuoteToolStripMenuItem
            // 
            this.generateQuoteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateQuoteToolStripMenuItem1,
            this.searchQuoteToolStripMenuItem});
            this.generateQuoteToolStripMenuItem.Name = "generateQuoteToolStripMenuItem";
            this.generateQuoteToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.generateQuoteToolStripMenuItem.Text = " Quote";
            this.generateQuoteToolStripMenuItem.Click += new System.EventHandler(this.generateQuoteToolStripMenuItem_Click);
            // 
            // generateQuoteToolStripMenuItem1
            // 
            this.generateQuoteToolStripMenuItem1.Name = "generateQuoteToolStripMenuItem1";
            this.generateQuoteToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.generateQuoteToolStripMenuItem1.Text = "Generate Quote";
            this.generateQuoteToolStripMenuItem1.Click += new System.EventHandler(this.generateQuoteToolStripMenuItem1_Click);
            // 
            // searchQuoteToolStripMenuItem
            // 
            this.searchQuoteToolStripMenuItem.Name = "searchQuoteToolStripMenuItem";
            this.searchQuoteToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.searchQuoteToolStripMenuItem.Text = "Search Quote";
            this.searchQuoteToolStripMenuItem.Click += new System.EventHandler(this.searchQuoteToolStripMenuItem_Click);
            // 
            // systemFunctionToolStripMenuItem
            // 
            this.systemFunctionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emailConfigurationToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.changePasswordToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.systemFunctionToolStripMenuItem.Name = "systemFunctionToolStripMenuItem";
            this.systemFunctionToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.systemFunctionToolStripMenuItem.Text = "System Functions";
            // 
            // emailConfigurationToolStripMenuItem
            // 
            this.emailConfigurationToolStripMenuItem.Name = "emailConfigurationToolStripMenuItem";
            this.emailConfigurationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.emailConfigurationToolStripMenuItem.Text = "Email Configuration";
            this.emailConfigurationToolStripMenuItem.Click += new System.EventHandler(this.emailConfigurationToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.closeAllToolStripMenuItem.Text = "Close All";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.closeAllToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "Quote Generation Tool";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mastersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rateMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateQuoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateQuoteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem searchQuoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemFunctionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}