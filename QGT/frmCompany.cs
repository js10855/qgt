﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmCompany : Form
    {
        string path = Path.GetFullPath(@"Master Data\Company.xlsx");
        public frmCompany()
        {
            InitializeComponent();
            FetchData();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtCompanyName.Text = "";
            txtAddress.Text = "";            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCompanyName.Text.Trim()) || string.IsNullOrEmpty(txtAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter both company name and address");
            }
            else
            {                
                string query = "update [Sheet1$] set CompanyName='" + txtCompanyName.Text + "',Address='" + txtAddress.Text + "' where CompanyId=1";
                DataAccessManager.UpdateData(path, query);
                MessageBox.Show("Data updated successfully");
            }
        }

        private void FetchData()
        {
            string query = "Select CompanyName, Address from [Sheet1$]";
            DataTable dt= DataAccessManager.FetchData(path, query);
            txtCompanyName.Text = dt.Rows[0]["CompanyName"].ToString() ;
            txtAddress.Text = dt.Rows[0]["Address"].ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
