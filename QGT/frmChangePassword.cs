﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmChangePassword : Form
    {
        string path = Path.GetFullPath(@"Master Data\Temp.xlsx");
        public frmChangePassword()
        {
            InitializeComponent();           
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtOldPassword.Text = "";
            txtNewPassword.Text = "";
            txtReEnterPassword.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                string newPwd = EncryptionManager.EncodeTo64(txtNewPassword.Text.Trim());
                string query = "Update [sheet1$] set Temp2='" + newPwd + "' where TempID=1";
                DataAccessManager.UpdateData(path, query);
                MessageBox.Show("Password updated successfully");
            } 
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtOldPassword.Text.Trim()) || string.IsNullOrEmpty(txtNewPassword.Text.Trim()) || string.IsNullOrEmpty(txtReEnterPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter all values");
                return false;
            }

            if (txtNewPassword.Text.Trim() != txtReEnterPassword.Text.Trim())
            {
                MessageBox.Show("New Password and Re Enter Password must be same");
                return false;
            }

            
            string query = "Select Temp2 from [Sheet1$]";
            DataTable dt = DataAccessManager.FetchData(path, query);
            string oldPwd = EncryptionManager.DecodeFrom64(dt.Rows[0]["Temp2"].ToString());

            if (txtOldPassword.Text.Trim() != oldPwd)
            {
                MessageBox.Show("Old Password is not correct");
                return false;
            }
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
