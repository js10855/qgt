﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmProcess : Form
    {
        public List<ProcessEntity> processList = new List<ProcessEntity>();
        public frmProcess()
        {
            InitializeComponent();            
        }

        private void LoadGrid()
        {
            if (processList.Count>0 )
            {
                dgvProcess.Columns.Add("ProcessName", "ProcessName");
                dgvProcess.Columns.Add("Value", "Value");
                dgvProcess.Columns.Add("Unit", "Unit");
                dgvProcess.Columns.Add("Rate", "Rate");
                dgvProcess.Columns.Add("Cost", "Cost");

                foreach (ProcessEntity process in processList)
                {
                    dgvProcess.Rows.Add(process.Select, process.ProcessName, process.Value, process.Unit, process.Rate, process.Value);
                }
                //dgvProcess.DataSource = processList;
                Calculate();
            }
            else
            { 
                string path = Path.GetFullPath(@"Master Data\Process.xlsx");
                string query = "SELECT ProcessName,Value,Unit,Rate,Cost FROM [Sheet1$] where type='P' order by [ProcessID] asc";
                dgvProcess.DataSource = DataAccessManager.FetchData(path, query);
            }
            dgvProcess.Columns["ProcessName"].ReadOnly = true;
            dgvProcess.Columns["Unit"].ReadOnly = true;
        }

        public void SetInitialValue()
        {
            lblRawMaterial.Text = RawMaterial;
            lblECost.Text = lblExistingCost.Text = RawMaterialCost;
        }
        #region Properties
        public string RawMaterial { get; set; }
        public string RawMaterialCost { get; set; }
        public decimal TotalProcessCost { get; set; }
        public decimal GrandTotal { get; set; }
        
        #endregion

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            FillDataInList();
            Calculate();            
        }

        private void Calculate()
        {
            decimal totalPrCost = 0;
            for (int i = 0; i < dgvProcess.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvProcess["Select", i].Value) == true && (!string.IsNullOrEmpty(dgvProcess["Value", i].Value.ToString())) && (!string.IsNullOrEmpty(dgvProcess["Rate", i].Value.ToString())))
                {
                    dgvProcess["Cost", i].Value = Convert.ToDecimal(dgvProcess["Value", i].Value) * Convert.ToDecimal(dgvProcess["Rate", i].Value);
                    totalPrCost = totalPrCost + Convert.ToDecimal(dgvProcess["Cost", i].Value);
                }
            }
            lblTotalCost.Text = totalPrCost.ToString();
            TotalProcessCost = totalPrCost;
            lblGrandTotal.Text = Math.Round(Convert.ToDecimal(lblECost.Text) + totalPrCost, 2).ToString();
            GrandTotal = Math.Round(Convert.ToDecimal(lblECost.Text) + totalPrCost, 2);
        }

        private void FillDataInList()
        {
            processList.Clear();
            for (int i = 0; i < dgvProcess.Rows.Count; i++)
            {
                ProcessEntity process = new ProcessEntity();
                process.Select = Convert.ToBoolean(dgvProcess["Select", i].Value);
                process.ProcessName =dgvProcess["ProcessName", i].Value.ToString();
                process.Value = dgvProcess["Value", i].Value ==DBNull.Value? 0: Convert.ToDecimal(dgvProcess["Value", i].Value);
                process.Unit =dgvProcess["Unit", i].Value.ToString();
                process.Rate = dgvProcess["Rate", i].Value ==DBNull.Value?0: Convert.ToDecimal(dgvProcess["Rate", i].Value);
                process.Cost = dgvProcess["Cost", i].Value == DBNull.Value ? 0 : Convert.ToDecimal(dgvProcess["Cost", i].Value);
                processList.Add(process);                
            }
        }

        
        
        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvProcess_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgvProcess.IsCurrentCellDirty)
            {
                // This fires the cell value changed handler
                dgvProcess.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvProcess_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            if (dgvProcess.Columns[e.ColumnIndex].Name == "Value" || dgvProcess.Columns[e.ColumnIndex].Name == "Rate")
            {
                if (Convert.ToBoolean(dgvProcess["Select", e.RowIndex].Value) == true && (!string.IsNullOrEmpty(dgvProcess["Value", e.RowIndex].Value.ToString())) && (!string.IsNullOrEmpty(dgvProcess["Rate", e.RowIndex].Value.ToString())))
                {
                    dgvProcess["Cost", e.RowIndex].Value = Convert.ToDecimal(dgvProcess["Value", e.RowIndex].Value) * Convert.ToDecimal(dgvProcess["Rate", e.RowIndex].Value);
                }                
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvProcess.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvProcess["Select", i].Value) == true)
                {
                    dgvProcess["Value", i].Value = 0;
                    dgvProcess["Cost", i].Value = "";
                }
            }
        }

        private void frmProcess_Load(object sender, EventArgs e)
        {
            LoadGrid();
        }
    }
}
