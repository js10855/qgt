﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmEmailConfig : Form
    {
        string path = Path.GetFullPath(@"Master Data\EmailConfig.xlsx");
        public frmEmailConfig()
        {
            InitializeComponent();
            FetchData();
        }
        
        private void btnReset_Click(object sender, EventArgs e)
        {
            txtSMTPHost.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtEmailFrom.Text = "";
            txtMailSubject.Text = "";
            rtbMailBody.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSMTPHost.Text.Trim()) || string.IsNullOrEmpty(txtUserName.Text.Trim()) || string.IsNullOrEmpty(txtPassword.Text.Trim()) || string.IsNullOrEmpty(txtEmailFrom.Text.Trim()) || string.IsNullOrEmpty(txtMailSubject.Text.Trim()) || string.IsNullOrEmpty(rtbMailBody.Text.Trim()))
            {
                MessageBox.Show("Please enter all values");
            }
            else
            {
                string query = "update [Sheet1$] set SMTPHost='" + txtSMTPHost.Text + "',UserName='" + txtUserName.Text + "',[Password]='" + txtPassword.Text + "',EmailFrom='" + txtEmailFrom.Text + "',MailSubject='" + txtMailSubject.Text + "',MailBody='" + rtbMailBody.Text + "' where ConfigId=1";
                DataAccessManager.UpdateData(path, query);
                MessageBox.Show("Data updated successfully");
            }
        }

        private void FetchData()
        {
            string query = "Select SMTPHost, UserName,Password,EmailFrom,MailSubject,MailBody from [Sheet1$]";
            DataTable dt= DataAccessManager.FetchData(path, query);
            txtSMTPHost.Text = dt.Rows[0]["SMTPHost"].ToString();
            txtUserName.Text = dt.Rows[0]["UserName"].ToString();
            txtPassword.Text = dt.Rows[0]["Password"].ToString();
            txtEmailFrom.Text = dt.Rows[0]["EmailFrom"].ToString();
            txtMailSubject.Text = dt.Rows[0]["MailSubject"].ToString();
            rtbMailBody.Text = dt.Rows[0]["MailBody"].ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
