﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace QGT
{
    class DataAccessManager
    {
        public static DataTable FetchData(string path,string query)
        {            
            string conString = @"Provider=Microsoft.ACE.OLEDB.11.0;" + "Data Source=" + path + ";" + "Extended Properties=" + "\"" + "Excel 12.0;HDR=YES;" + "\"";
            OleDbConnection oleCon = new OleDbConnection(conString);
            OleDbCommand oleCmd = new OleDbCommand(query, oleCon);
            DataTable dt = new DataTable();
            oleCon.Open();
            dt.Load(oleCmd.ExecuteReader());
            oleCon.Close();
            return dt;
        }

        public static void UpdateData(string path, string query)
        {
            string conString = @"Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + path + ";" + "Extended Properties=" + "\"" + "Excel 12.0;HDR=YES;" + "\"";
            OleDbConnection oleCon = new OleDbConnection(conString);
            OleDbCommand oleCmd = new OleDbCommand(query, oleCon);
            oleCon.Open();
            oleCmd.ExecuteNonQuery();            
            oleCon.Close();            
        }
    }
}
