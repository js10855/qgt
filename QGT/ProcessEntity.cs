﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QGT
{
    public class ProcessEntity
    {
        public bool Select{get;set;}
        public string ProcessName { get; set; }
        public decimal Value { get; set; }
        public string Unit { get; set; }
        public decimal Rate { get; set; }
        public decimal Cost { get; set; }
    }
}
