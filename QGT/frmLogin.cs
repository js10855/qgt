﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmLogin : Form
    {
        string path = Path.GetFullPath(@"Master Data\Temp.xlsx");
        public frmLogin()
        {
            InitializeComponent();
            ResetData();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtUserName.Focus();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUserName.Text.Trim()) || string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter both username and password");
            }
            else
            {
                string query = "Select Temp1,Temp2 from [Sheet1$]";
                DataTable dt = DataAccessManager.FetchData(path, query);
                string uName = EncryptionManager.DecodeFrom64(dt.Rows[0]["Temp1"].ToString());
                string Pwd = EncryptionManager.DecodeFrom64(dt.Rows[0]["Temp2"].ToString());

                if (txtUserName.Text == uName && txtPassword.Text ==Pwd)
                {
                    frmMain frmM = new frmMain();
                    frmM.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("User name or Password is incorrect");
                }
            }

        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                btnLogIn_Click(sender, e);
        }

        private void frmLogin_Activated(object sender, EventArgs e)
        {
            ResetData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
