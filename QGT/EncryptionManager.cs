﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QGT
{
    class EncryptionManager
    {
        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string DecodeFrom64(string encodedData)
        {
            string returnValue = "";
            if (!string.IsNullOrEmpty(encodedData))
            {
                encodedData = encodedData.Replace(",", "");
                byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
                returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);
            }
            return returnValue;
        }
    }
}
