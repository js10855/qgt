﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmContact : Form
    {
        string path = Path.GetFullPath(@"Master Data\Customer.xlsx");
        public frmContact()
        {
            InitializeComponent();                       
        }

        #region "Properties"
        public string CustomerName { get; set; }
        public int CustomerID { get; set; }
        #endregion

        private void FillGrid()
        {
            string query = "Select ContactID,ContactName as [Contact Name],EmailID as [Email ID] from [sheet2$] where CustomerID='" + CustomerID + "'";
            DataTable dt = DataAccessManager.FetchData(path, query);   
            dgvContact.DataSource = dt;            
            dgvContact.Columns["ContactID"].Visible = false;            
        }

        private void SetGridFirstTime()
        {
            DataGridViewImageColumn save = new DataGridViewImageColumn();
            save.HeaderText = "Save";
            save.Name = "Save";
            save.Image = Properties.Resources.Save;
            dgvContact.Columns.Add(save);           
        }

        private void frmContact_Load(object sender, EventArgs e)
        {
            lblCust.Text = CustomerName;
            FillGrid();
            SetGridFirstTime();            
        }

       

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtContactName.Text = "";
            txtEmail.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtContactName.Text.Trim()) && string.IsNullOrEmpty(txtEmail.Text.Trim()))
            {
                MessageBox.Show("Please enter at least one value to save entry");
            }
            else
            {
                int conID=0;
                string query = "Select max(ContactID) as ContactID from [sheet2$] ";
                DataTable dt = DataAccessManager.FetchData(path, query);
                //conID = Convert.ToInt32(dt.Rows[0]["ContactID"]) + 1;

                if (!string.IsNullOrEmpty(dt.Rows[0]["ContactID"].ToString()))
                {
                    conID = Convert.ToInt32(dt.Rows[0]["ContactID"]) + 1;
                }
                else
                {
                    conID += 1;
                }


                query = "Insert into [sheet2$] " +
                    "(CustomerID,ContactID,ContactName,EmailID) values( " +
                    CustomerID + "," + conID + ",'" + txtContactName.Text + "','" + txtEmail.Text + "')";

                DataAccessManager.UpdateData(path, query);

                MessageBox.Show("Contact Record added successfully");
                FillGrid();
            }
        }

        private void dgvContact_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            if (dgvContact.Columns[e.ColumnIndex].Name == "Save") 
                dgvContact.Cursor = Cursors.Hand;
            else
                dgvContact.Cursor = Cursors.Default;
        }

        private void dgvContact_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            if (dgvContact.Columns[e.ColumnIndex].Name == "Save")
            {
                string query = "Update [Sheet2$] set ContactName ='" + dgvContact["Contact Name", e.RowIndex].Value + "'," +
                               "EmailID='" + dgvContact["Email ID", e.RowIndex].Value +
                "' where contactID='" + dgvContact["ContactID", e.RowIndex].Value + "' and " +
                "customerID='" + CustomerID + "'" ;
                DataAccessManager.UpdateData(path, query);
                MessageBox.Show("Contact Record saved successfully");
                //ChangeColumnState(true);
            }
        }
    }
}
