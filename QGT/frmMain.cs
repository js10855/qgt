﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace QGT
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void generateQuoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void generateQuoteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmQuoteGeneration frmQuote = new frmQuoteGeneration();
            frmQuote.Show();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "frmLogin")
                    Application.OpenForms[i].Close();
                else
                    Application.OpenForms[i].Show();
            }
            //frmLogin frmlogin = new frmLogin();
            //frmlogin.Show();
            //Application.OpenForms["frmMain"].Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "frmMain" && Application.OpenForms[i].Name != "frmLogin")
                    Application.OpenForms[i].Close();
            }
        }

        private void companyMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCompany frmC = new frmCompany();
            frmC.Show();
        }

        private void emailConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEmailConfig frmE = new frmEmailConfig();
            frmE.Show();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmChangePassword frmC = new frmChangePassword();
            frmC.Show();
        }

        private void customerMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCustomer frmC = new frmCustomer();
            frmC.Show();
        }

        private void rateMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRate frmR = new frmRate();
            frmR.Show();
        }

        private void searchQuoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmQuoteSelection frmQ = new frmQuoteSelection();
            frmQ.ParentName = "Main";
            frmQ.Show();
        }
    }
}
