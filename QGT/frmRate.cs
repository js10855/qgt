﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmRate : Form
    {
        string path = Path.GetFullPath(@"Master Data\Process.xlsx");
        public frmRate()
        {
            InitializeComponent();
            LoadGrid();
        }
        private void LoadGrid()
        {            
            string query = "SELECT ProcessID,ProcessName as [Parameter],Unit,Rate FROM [Sheet1$] order by [ProcessID] asc";
            dgvRates.DataSource = DataAccessManager.FetchData(path, query);
            dgvRates.Columns["ProcessID"].Visible = false;
            dgvRates.Columns["Parameter"].ReadOnly = true;
            dgvRates.Columns["Unit"].ReadOnly = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            for (int i=0;i<dgvRates.Rows.Count;i++)
            {
                string query;
                if (!string.IsNullOrEmpty(dgvRates["Rate", i].Value.ToString()))
                { 
                     query = "Update [Sheet1$] set Rate =" + dgvRates["Rate", i].Value +
                                   " where ProcessID=" + dgvRates["ProcessID", i].Value; 
                    
                }
                else
                {
                     query = "Update [Sheet1$] set Rate =''" +
                                   " where ProcessID=" + dgvRates["ProcessID", i].Value; 
                }
                DataAccessManager.UpdateData(path, query);
            }
            LoadGrid();
            MessageBox.Show("Rates updated successfully");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvRates.Rows.Count; i++)
            {
                dgvRates["Rate", i].Value = "";
            }
        }
    }
}
