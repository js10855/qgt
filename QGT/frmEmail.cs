﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;

namespace QGT
{
    public partial class frmEmail : Form
    {
        string path = Path.GetFullPath(@"Master Data\Customer.xlsx");
        public frmEmail()
        {
            InitializeComponent();
        }
        
        #region "Properties"
        public string CustomerName { get; set; }
        public int CustomerID { get; set; }

        public string FileName { get; set; }

        public string PrimaryContact { get; set; }
        #endregion

        private void frmEmail_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            string query = "Select ContactID,ContactName as [Contact Name],EmailID as [Email ID] from [sheet2$] where CustomerID='" + CustomerID + "'";
            DataTable dt = DataAccessManager.FetchData(path, query);
            dgvEmail.DataSource = dt;
            dgvEmail.Columns["ContactID"].Visible = false;

            
            for (int i = 0; i < dgvEmail.Rows.Count; i++)
            {
                if (PrimaryContact == Convert.ToString(dgvEmail["Contact Name", i].Value.ToString()))
                    dgvEmail["Select", i].Value = true;
            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            DataTable dt = LoadEmailConfig();

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = dt.Rows[0]["SMTPHost"].ToString();
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(dt.Rows[0]["UserName"].ToString(), dt.Rows[0]["Password"].ToString());
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            MailMessage mailToAdmin = new MailMessage();
            mailToAdmin.From = new System.Net.Mail.MailAddress(dt.Rows[0]["EmailFrom"].ToString());
            mailToAdmin.Subject = dt.Rows[0]["MailSubject"].ToString();
            mailToAdmin.IsBodyHtml = true;
            mailToAdmin.Body = dt.Rows[0]["MailBody"].ToString();
            mailToAdmin.To.Add(CustomerEmailList());
            mailToAdmin.Attachments.Add(new Attachment(FileName));
            try
            {
                smtpClient.Send(mailToAdmin);
                MessageBox.Show("Email has been sent successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("error sending email: " + ex.Message);
            } 
        }

        private string CustomerEmailList()
        {
            string emailList ="";
            for (int i=0;i< dgvEmail.Rows.Count-1;i++ )
            {
                if (Convert.ToBoolean(dgvEmail["Select",i].Value) ==true)
                {
                    emailList = emailList + dgvEmail["Email ID", i].Value.ToString() + ",";
                }
            }
            return emailList;
        }

        private DataTable LoadEmailConfig()
        {
            string path = Path.GetFullPath(@"Master Data\EmailConfig.xlsx");
            string query = "SELECT SMTPHost,UserName,Password,EmailFrom,MailSubject,MailBody FROM [Sheet1$] where ConfigID=1";
            return DataAccessManager.FetchData(path, query);
        }
               

        private void btnReset_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvEmail.Rows.Count; i++)
            {
                dgvEmail["Select", i].Value = false;
            }
        }
    }
}
