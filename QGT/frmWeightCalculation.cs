﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace QGT
{
    public partial class frmWeightCalculation : Form
    {
        private string formulaID;
        private string noOfFields;
        private string internalFormula;
        
        public frmWeightCalculation()
        {
            InitializeComponent();           
        }
        
        public string RawMaterial { get; set; }
        public decimal WeightValue {get;set;}

        public string ParameterValue { get; set; }
        
        public void FindFormula()
        {
            DataTable dtFormula = new DataTable();
            string path = Path.GetFullPath(@"Master Data\Formula.xlsx");
            string query = "SELECT FormulaID , RawMaterial,Formula,InternalFormula,NoOfFields FROM [Sheet1$] order by FormulaID";
            dtFormula = DataAccessManager.FetchData(path, query);            
            lblRawMat.Text = RawMaterial;
            DataRow[] foundRows = dtFormula.Select("RawMaterial=" + "'" + RawMaterial + "'");
            lblFormula.Text = foundRows[0][2].ToString();
            internalFormula = foundRows[0][3].ToString();
            formulaID =  foundRows[0][0].ToString();
            noOfFields = foundRows[0][4].ToString();
            HideParameters();
            SetLabels();
            SetValues();
            //Calculate();
        }

        private void SetLabels()
        {
            DataTable dtFormula = new DataTable();
            string path = Path.GetFullPath(@"Master Data\Formula.xlsx");
            string query =  "SELECT FormulaID , Parameters,Unit FROM [Sheet2$] where FormulaID=" + formulaID + " order by OrderID";
            dtFormula = DataAccessManager.FetchData(path, query);
            
            for (int i = 0; i != dtFormula.Rows.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPara1.Text = dtFormula.Rows[i][1].ToString();
                        lblUnit1.Text = dtFormula.Rows[i][2].ToString();
                        break;
                    case 1:
                        lblPara2.Text = dtFormula.Rows[i][1].ToString();
                        lblUnit2.Text = dtFormula.Rows[i][2].ToString();
                        break;
                    case 2:
                        lblPara3.Text = dtFormula.Rows[i][1].ToString();
                        lblUnit3.Text = dtFormula.Rows[i][2].ToString();
                        break;
                    case 3:
                        lblPara4.Text = dtFormula.Rows[i][1].ToString();
                        lblUnit4.Text = dtFormula.Rows[i][2].ToString();
                        break;
                    default:
                        break;

                }
            }

        }

        private void HideParameters()
        {
            switch (noOfFields)
                {
                case "1":                        
                        break; //this case does not exist currently
                case "2":
                        lblPara3.Visible = false;
                        txtPara3.Visible = false;
                        lblUnit3.Visible = false;
                        lblPara4.Visible = false;
                        txtPara4.Visible = false;
                        lblUnit4.Visible = false;
                        break;
                case "3":
                        lblPara4.Visible = false;
                        txtPara4.Visible = false;
                        lblUnit4.Visible = false;
                        break;
                default:
                        break;
                }

        }

        private void SetValues()
        {
            if (!string.IsNullOrEmpty(ParameterValue))
            {
                string[] values = ParameterValue.Split(',');
                if (values.Length > 0)
                    txtPara1.Text = values[0];
                if (values.Length > 1)
                    txtPara2.Text = values[1];
                if (values.Length > 2)
                    txtPara3.Text = values[2];
                if (values.Length > 3)
                    txtPara4.Text = values[3];

            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            StartCalculate();
        }

        private void StartCalculate()
        {
            if (ValidateData())
            {
                Calculate();
            }
        }

        public void Calculate()
        {
            ParameterValue = txtPara1.Text + "," + txtPara2.Text + "," + txtPara3.Text + "," + txtPara4.Text;

            string tempFormula = internalFormula;
            internalFormula = internalFormula.Replace("val1", txtPara1.Text);
            internalFormula = internalFormula.Replace("val2", txtPara2.Text);
            internalFormula = internalFormula.Replace("val3", txtPara3.Text);
            internalFormula = internalFormula.Replace("val4", txtPara4.Text);

            DataTable dt = new DataTable();
            //lblSizeValue.Text = "Actual Value: " + dt.Compute(internalFormula, "") + " pcs \nRound-off:" + Convert.ToUInt32(dt.Compute(internalFormula, "")) + " pcs";
            //lblSizeValue.Text = Evaluate(internalFormula).ToString();

            lblSizeValue.Text = Math.Round(Convert.ToDecimal(dt.Compute(internalFormula, "")), 3) + " pcs ";
            WeightValue = Math.Round(Convert.ToDecimal(dt.Compute(internalFormula, "")), 3);

            internalFormula = tempFormula;
            lblSize.Visible = true;
            lblSizeValue.Visible = true;
        }

        //private decimal Evaluate(string expression)
        //{
        //    var loDataTable = new DataTable();
        //    var loDataColumn = new DataColumn("Eval", typeof(decimal), expression);
        //    loDataTable.Columns.Add(loDataColumn);
        //    loDataTable.Rows.Add(0);
        //    return (decimal)(loDataTable.Rows[0]["Eval"]);
        //}

        public static decimal Evaluate(string expression)
        {
            return (decimal)new System.Xml.XPath.XPathDocument
            (new StringReader("<r/>")).CreateNavigator().Evaluate
            (string.Format("number({0})", new
            System.Text.RegularExpressions.Regex(@"([\+\-\*])")
            .Replace(expression, " ${1} ")
            .Replace("/", " div ")
            .Replace("%", " mod ")));
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();           
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtPara1.Text.Trim()) || string.IsNullOrEmpty(txtPara2.Text.Trim()))
            {   MessageBox.Show("Please enter values");    return false; }

            if (txtPara3.Visible == true && string.IsNullOrEmpty(txtPara3.Text.Trim()))
            {   MessageBox.Show("Please enter values");  return false;  }

            if (txtPara4.Visible == true && string.IsNullOrEmpty(txtPara4.Text.Trim()))
            { MessageBox.Show("Please enter values"); return false; }

            decimal result;
            if (!decimal.TryParse(txtPara1.Text.Trim(), out result) || !decimal.TryParse(txtPara2.Text.Trim(), out result))
            { MessageBox.Show("Please enter numeric values only"); return false; }

            if (txtPara3.Visible == true && !decimal.TryParse(txtPara3.Text.Trim(), out result))
            { MessageBox.Show("Please enter numeric values only"); return false; }

            if (txtPara4.Visible == true && !decimal.TryParse(txtPara4.Text.Trim(), out result))
            { MessageBox.Show("Please enter numeric values only"); return false; }

            return true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtPara1.Text = "";
            txtPara2.Text = "";
            txtPara3.Text = "";
            txtPara4.Text = "";
        }
    }
}
