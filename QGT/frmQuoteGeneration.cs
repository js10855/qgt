﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.IO;
using Excel =Microsoft.Office.Interop.Excel;


namespace QGT
{
    public partial class frmQuoteGeneration : Form
    {
        #region "Private variables"
        List<ItemProcessEntity> ltItems = new List<ItemProcessEntity>(); 
        private int itemID;
        private decimal valOtherCharges { get; set; }
        private decimal valProfit { get; set; }
        #endregion
        public frmQuoteGeneration()
        {
            InitializeComponent();
            BindCustomerData();
            itemID = -1;
            dgvItems.Rows.Add();
            cmbCustomer.SelectedValue = 0;
            txtQuoteNo.Text = "";
            cmbQuote.Text = "New Quote";
        }
        #region "Events"

        private void cmbQuote_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbQuote.SelectedItem.ToString() == "An Existing Quote")
            {
                frmQuoteSelection frmQuotes = new frmQuoteSelection();
                frmQuotes.ShowDialog();  
                if (!string.IsNullOrEmpty(frmQuotes.CustomerName))
                { 
                    cmbCustomer.Text = frmQuotes.CustomerName;
                    dtDate.Value = frmQuotes.QuoteDate;
                    txtQuoteNo.Text = frmQuotes.QuoteNo;
                    cmbContact.Text = frmQuotes.PrimaryContact;
                    CopyExistingQuote();
                }
            }
        }
                
        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {       
           if (!string.IsNullOrEmpty(cmbCustomer.Text))
           { 
            txtQuoteNo.Text = "JEW/" + (cmbCustomer.Text).Substring(0, 3).ToUpper() + "/" + GetNewQuoteNo() + "/" + nRevision.Value;

            string path = Path.GetFullPath(@"Master Data\Customer.xlsx");
            string query = "Select ContactID,ContactName from [sheet2$] where CustomerID='" + cmbCustomer.SelectedValue + "'";
            cmbContact.DataSource = DataAccessManager.FetchData(path, query);
            
            cmbContact.ValueMember = "ContactID";
            cmbContact.DisplayMember = "ContactName";

            query = "Select Address from [sheet1$] where CustomerID='" + cmbCustomer.SelectedValue + "'";
            DataTable dt =DataAccessManager.FetchData(path, query);
            if (dt.Rows.Count>0)
                txtAddress.Text = dt.Rows[0]["Address"].ToString();
           }
        }      

        private void dgvItems_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            switch (dgvItems.Columns[e.ColumnIndex].Name)
            {
                case "Add": { dgvItems.Rows.Add(); break; }
                case "Delete": { RemoveFromList(Convert.ToInt32(dgvItems["itemID", e.RowIndex].Value)); dgvItems.Rows.RemoveAt(e.RowIndex); break; }                
                case "WeightCalculate":
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dgvItems["RawMaterial", e.RowIndex].Value)))
                        {
                            MessageBox.Show("Please enter Raw Material first");
                        }
                        else
                        {
                            frmWeightCalculation frmWeight = new frmWeightCalculation();
                            frmWeight.RawMaterial = dgvItems["RawMaterial", e.RowIndex].Value.ToString();
                            frmWeight.ParameterValue = Convert.ToString(dgvItems["Size", e.RowIndex].Value);
                            frmWeight.FindFormula();
                            if (!string.IsNullOrEmpty(frmWeight.ParameterValue))
                                { frmWeight.Calculate(); }
                            frmWeight.ShowDialog();
                            //After screen display
                            dgvItems["Weight", e.RowIndex].Value = frmWeight.WeightValue;
                            dgvItems["Size", e.RowIndex].Value = frmWeight.ParameterValue;
                            Calculate(e.RowIndex);                            
                        }
                        break;
                    }
                case "Process":
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dgvItems["RawMaterial", e.RowIndex].Value)))
                        {
                            MessageBox.Show("Please enter Raw Material first");
                        }
                        else
                        {                            
                            frmProcess frmProcess = new frmProcess();
                            frmProcess.RawMaterial = dgvItems["RawMaterial", e.RowIndex].Value.ToString();
                            frmProcess.RawMaterialCost = dgvItems["Cost", e.RowIndex].Value.ToString();
                            frmProcess.SetInitialValue();                                                      
                            SetList(e.RowIndex,ref frmProcess.processList);
                            frmProcess.ShowDialog();
                            //After screen display
                            dgvItems["ProcessCost", e.RowIndex].Value = frmProcess.TotalProcessCost ;
                            Calculate(e.RowIndex);                            
                            FillList(frmProcess.processList, Convert.ToInt32(dgvItems["itemID", e.RowIndex].Value));
                        }
                        break;
                    }
            }
        }
        
        private void dgvItems_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            if (dgvItems.Columns[e.ColumnIndex].Name == "Delete")
                dgvItems.Cursor = Cursors.Hand;
            else
                dgvItems.Cursor = Cursors.Default;
        }
       
        private void dgvItems_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            DataGridViewComboBoxColumn rawMatCombo = (DataGridViewComboBoxColumn)(dgvItems.Columns["RawMaterial"]);
            rawMatCombo.DataSource = LoadRawMatInDataTable();
            rawMatCombo.DisplayMember = "RawMaterial";

            dgvItems["Quantity", e.RowIndex].Value = 1;
            itemID += 1;
            dgvItems["itemID", e.RowIndex].Value = itemID;           
        }

        private void dgvItems_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }            

            switch (dgvItems.Columns[e.ColumnIndex].Name)
            {
                case "Quantity": case "Rate":
                    {                        
                        Calculate(e.RowIndex);
                        break;
                    }               
                case "RawMaterial":
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dgvItems["RawMaterial", e.RowIndex].Value)))
                        {
                            dgvItems["Rate", e.RowIndex].Value = FindRate(Convert.ToString(dgvItems["RawMaterial", e.RowIndex].Value));
                        }
                        dgvItems["Size", e.RowIndex].Value = "";
                        dgvItems["Weight", e.RowIndex].Value = 0;
                        break;
                    }
            }        
        }

        private void dgvItems_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgvItems.IsCurrentCellDirty)
            {
                // This fires the cell value changed handler
                dgvItems.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }

        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                string fileName = GenerateDoc(true, false);
                System.Diagnostics.Process.Start(fileName);
            }
        }
        private void btnPDF_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                string fileName = GenerateDoc(true, true);
                System.Diagnostics.Process.Start(fileName);
            }
        }
        private void btnEmail_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {                
                frmEmail frmMail = new frmEmail();
                frmMail.CustomerID = Convert.ToInt32(cmbCustomer.SelectedValue);
                frmMail.CustomerName = cmbCustomer.Text;
                frmMail.PrimaryContact = cmbContact.Text;
                string fileName = GenerateDoc(true, true);
                frmMail.FileName = fileName;
                frmMail.ShowDialog();
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            { 
                if (GenerateDoc(false, false) != "Error") 
                {
                    SaveDataInQuoteList();
                    MessageBox.Show("Quote saved successfully");
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbCustomer.Text))
                txtQuoteNo.Text = "JEW/" + (cmbCustomer.Text).Substring(0, 3).ToUpper() + "/" + GetNewQuoteNo() + "/" + nRevision.Value;
        }

        private void btnSyncRevision_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbCustomer.Text) && (!string.IsNullOrEmpty(txtQuoteNo.Text)))
            { 
                string qNo = txtQuoteNo.Text.Substring(0,txtQuoteNo.Text.LastIndexOf("/"));
                txtQuoteNo.Text = qNo + "/" + nRevision.Value;
            }
        }

        #endregion

        #region "Methods"    
        private int GetNewQuoteNo()
        {
            int quoteID = 0;
            string path = Path.GetFullPath(@"Quote List\Quote List.xlsx");
            string query = "Select max(QuoteID) as QuoteID from [sheet1$] ";
            DataTable dt = DataAccessManager.FetchData(path, query);
            quoteID = (!string.IsNullOrEmpty(dt.Rows[0]["QuoteID"].ToString())) ? Convert.ToInt32(dt.Rows[0]["QuoteID"]) + 1 : quoteID + 1;
            return quoteID;
        }
        private void SaveDataInQuoteList()
        {
            int quoteID = 0;
            decimal totalAmt = 0;

            for (int i=0;i<dgvItems.Rows.Count;i++)
            {
                totalAmt = totalAmt + Convert.ToDecimal(dgvItems["FinalCost", i].Value);
            }

            quoteID = GetNewQuoteNo();

            string path = Path.GetFullPath(@"Quote List\Quote List.xlsx");
            string query = "Select QuoteNo from [sheet1$] where quoteNo='" + txtQuoteNo.Text + "'";
            DataTable dt= DataAccessManager.FetchData(path, query);

            if (dt.Rows.Count >0)
            { 
                query = "Update [sheet1$] set [Date]='" + dtDate.Value + "'," +
                        "Customer='" + cmbCustomer.Text + "'," +
                        "TotalAmount=" + totalAmt + ",PrimaryContact='" + cmbContact.Text + "' where quoteNo='" + txtQuoteNo.Text + "'";
            }
            else
            { 
                query = "Insert into [sheet1$]" +
                        "(QuoteID,QuoteNo,[Date],Customer,TotalAmount,PrimaryContact) values( " +
                        quoteID + ",'" + txtQuoteNo.Text + "','" + dtDate.Value + "','" + cmbCustomer.Text + "'," + totalAmt + ",'" + cmbContact.Text + "')";                
            }
            DataAccessManager.UpdateData(path, query);
        }
        private void SetList(int rowNum,ref List<ProcessEntity> processList)
        {
            foreach (ItemProcessEntity id in ltItems)
            {
                if (id.ItemID == Convert.ToInt32(dgvItems["itemID", rowNum].Value))
                {                    
                    processList = id.processList;
                    break;
                }
            }            
        }
        private void FillList(List<ProcessEntity> processList, int row)
        {
            RemoveFromList(row);
            ItemProcessEntity itProcessList = new ItemProcessEntity();
            itProcessList.ItemID = row; 
            itProcessList.processList = processList;
            ltItems.Add(itProcessList);
        }
        private void RemoveFromList(int row)
        {
            foreach (ItemProcessEntity id in ltItems)
            {
                if (id.ItemID == row)
                {
                    ltItems.Remove(id);
                    break;
                }
            }
        }
        private void Calculate(int rowNum)
        {
            dgvItems["Cost", rowNum].Value = Math.Round(Convert.ToDecimal(dgvItems["Quantity", rowNum].Value) * Convert.ToDecimal(dgvItems["Rate", rowNum].Value) * Convert.ToDecimal(dgvItems["Weight", rowNum].Value), 2);
            dgvItems["GrossCost", rowNum].Value = Math.Round(Convert.ToDecimal(dgvItems["Cost", rowNum].Value) + Convert.ToDecimal(dgvItems["ProcessCost", rowNum].Value), 2);
            findChargesAndProfit();
            dgvItems["OtherCharges", rowNum].Value = Math.Round(Convert.ToDecimal(dgvItems["GrossCost", rowNum].Value) * valOtherCharges / 100, 2);
            dgvItems["Profit", rowNum].Value = Math.Round(Convert.ToDecimal(dgvItems["GrossCost", rowNum].Value) * valProfit / 100, 2);
            dgvItems["FinalCost", rowNum].Value = Convert.ToDecimal(dgvItems["GrossCost", rowNum].Value) + Convert.ToDecimal(dgvItems["OtherCharges", rowNum].Value) + Convert.ToDecimal(dgvItems["Profit", rowNum].Value);
        }
        private void findChargesAndProfit()
        {
            string path = Path.GetFullPath(@"Master Data\Process.xlsx");
            string query = "SELECT Rate FROM [Sheet1$] where ProcessName='Other Charges' or ProcessName='Profit' order by [ProcessID] asc";
            DataTable dt = DataAccessManager.FetchData(path, query);
            valOtherCharges = Convert.ToDecimal(dt.Rows[0]["Rate"]);
            valProfit = Convert.ToDecimal(dt.Rows[1]["Rate"]);
        }
        private decimal FindRate(string rawMat)
        {
            string path = Path.GetFullPath(@"Master Data\Process.xlsx");
            string query = "SELECT Rate FROM [Sheet1$] where ProcessName='" + rawMat + "'";
            DataTable dt = DataAccessManager.FetchData(path, query);
            if (string.IsNullOrEmpty(dt.Rows[0]["Rate"].ToString()))
                return 0;
            else
                return Convert.ToDecimal(dt.Rows[0]["Rate"]);
        }
        private void BindCustomerData()
        {
            string path = Path.GetFullPath(@"Master Data\Customer.xlsx");
            string query = "SELECT CustomerID, CustomerName FROM [Sheet1$] order by CustomerName";
            cmbCustomer.DataSource = DataAccessManager.FetchData(path, query);
            
            cmbCustomer.ValueMember = "CustomerID";
            cmbCustomer.DisplayMember = "CustomerName";                              
        }
        private DataTable LoadRawMatInDataTable()
        {            
            string path = Path.GetFullPath(@"Master Data\Raw Material.xlsx");
            string query = "SELECT RawMatID, RawMaterial FROM [Sheet1$] order by RawMaterial";
            return DataAccessManager.FetchData(path, query);
        }           
        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        private string GenerateDoc(bool tempFile,bool reqPDF)
        {
            Excel.Application exApp = null;
            Excel.Workbook exBook = null;
            Excel.Worksheet exSheet = null;
            object misValue = System.Reflection.Missing.Value;

            exApp = new Excel.Application();
            exApp.Visible = false;
            exBook = exApp.Workbooks.Add(misValue);
            exSheet = (Excel.Worksheet)exBook.Sheets.get_Item(1);
            string fName;
            string fileName;
            int rawNum;
            //string quoteNo = txtQuoteNo.Text.Replace(@"/", "");
            if (tempFile == true)
            { 
                fName = DateTime.Now.ToString().Replace(@"/", "");
                fName = fName.Replace(@":", "");
                fileName = Path.GetFullPath(@"Temp Quotes\" + fName + ".xlsx");

                string path = Path.GetFullPath(@"Master Data\Company.xlsx");
                string query = "Select CompanyName,Address from [sheet1$] where CompanyID=1 ";
                DataTable dt = DataAccessManager.FetchData(path, query);
                       
                exSheet.Cells[1, 1] = dt.Rows[0]["CompanyName"];
                exSheet.get_Range("A1", "D1").Merge();
                
                exSheet.Cells[2, 1] = dt.Rows[0]["Address"];
                exSheet.get_Range("A2", "D2").Merge();
                exSheet.Rows.AutoFit();
                exSheet.get_Range("A2", "D2").VerticalAlignment = Excel.XlVAlign.xlVAlignTop;

                exSheet.Cells[3, 1] = "Date:" + DateTime.Now.Date;
                exSheet.get_Range("A3", "D3").Merge();

                exSheet.Cells[4, 1] = "Quote No:" + txtQuoteNo.Text;
                exSheet.get_Range("A4", "D4").Merge();

                exSheet.Cells[6, 1] = "To,";
                exSheet.get_Range("A6", "D6").Merge();

                exSheet.Cells[7, 1] = cmbContact.Text;
                exSheet.get_Range("A7", "D7").Merge();

                exSheet.Cells[8, 1] = cmbCustomer.Text;
                exSheet.get_Range("A8", "D8").Merge();

                exSheet.Cells[9, 1] = txtAddress.Text;
                exSheet.get_Range("A9", "D9").Merge();

                rawNum = 11;
            }
            else
            {
                fName = txtQuoteNo.Text.Replace(@"/", ""); 
                fileName = Path.GetFullPath(@"Quote List\" + fName + ".xlsx");
                rawNum = 0;
            }
           
            SaveItemGridInExcel(ref exSheet,rawNum,tempFile);
            if (tempFile ==false)
            { 
                exSheet = (Excel.Worksheet)exBook.Sheets.get_Item(2);
                exSheet.Cells[1, 1] = "ItemID";
                exSheet.Cells[1, 2] = "Select";
                exSheet.Cells[1, 3] = "ProcessName";
                exSheet.Cells[1, 4] = "Value";
                exSheet.Cells[1, 5] = "Unit";
                exSheet.Cells[1, 6] = "Rate";
                exSheet.Cells[1, 7] = "Cost";

                int LastRow = 1;
                foreach (ItemProcessEntity item in ltItems)
                {
                    int col =1;
                    foreach(ProcessEntity pr in item.processList)
                    {
                        exSheet.Cells[LastRow + 1, col] = item.ItemID; col+=1;
                        exSheet.Cells[LastRow + 1, col] = pr.Select; col += 1;
                        exSheet.Cells[LastRow + 1, col] = pr.ProcessName; col += 1;
                        exSheet.Cells[LastRow + 1, col] = pr.Value; col += 1;
                        exSheet.Cells[LastRow + 1, col] = pr.Unit; col += 1;
                        exSheet.Cells[LastRow + 1, col] = pr.Rate; col += 1;
                        exSheet.Cells[LastRow + 1, col] = pr.Cost; col += 1;
                        LastRow += 1;
                        col = 1;
                    }                    
                }
            }
            else
            {
                exSheet.Cells[30, 1] = System.Configuration.ConfigurationSettings.AppSettings.Get("Quote1");
                exSheet.Cells[31, 1] = System.Configuration.ConfigurationSettings.AppSettings.Get("Quote2");
                exSheet.Cells[32, 1] = System.Configuration.ConfigurationSettings.AppSettings.Get("Quote3");  
                exSheet.Cells[33, 1] = System.Configuration.ConfigurationSettings.AppSettings.Get("Quote4");  
                exSheet.Cells[34, 1] = System.Configuration.ConfigurationSettings.AppSettings.Get("Quote5");  
            }

            try
            { 
                exBook.SaveAs(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show("File cannot be saved");
                return "Error";
            }

            if (reqPDF == true)
            {
                fileName = Path.GetFullPath(@"Temp Quotes\" + fName + ".pdf");
                object varMissing = Type.Missing;
                ((Microsoft.Office.Interop.Excel._Worksheet)
                exBook.ActiveSheet).PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                exBook.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, fileName,
                                            Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard,
                                            varMissing, false, varMissing, varMissing, false, varMissing);
            }

            exBook.Close(true, misValue, misValue);
            exApp.Quit();

            ReleaseObject(exSheet);
            ReleaseObject(exBook);
            ReleaseObject(exApp);

            return fileName;
        }

        private void SaveItemGridInExcel(ref Excel.Worksheet exSheet,int rawNum,bool tempFile)
        {
            object misValue = System.Reflection.Missing.Value;
            int lastRow = rawNum;
            int count = 0;
           
            //Header Row
            for (int i = 0; i < dgvItems.Columns.Count; i++)
            {
                if (tempFile == true)
                {
                    switch (dgvItems.Columns[i].Name)
                    {
                        case "Weight":
                        case "Quantity":
                        case "GrossCost":
                        case "RateExpected":
                        case "OtherCharges":
                        case "WeightCalculate":
                        case "Process":
                        case "Add":
                        case "Delete": break;
                        case "ItemID":
                            {
                                exSheet.Cells[lastRow + 1, count + 1] = "Sr#";
                                Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                                formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                                count += 1;                                
                                break;
                            }
                        default:
                        {
                        exSheet.Cells[lastRow + 1, count + 1] = dgvItems.Columns[i].HeaderText;
                        Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                        formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                        count += 1;
                        break;
                         }
                    }                   
                }
                else
                { 
                  exSheet.Cells[lastRow + 1, count + 1] = dgvItems.Columns[i].HeaderText;  
                  Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                  formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                  count += 1;
                }
            }
            lastRow += 1;
            //Header Row

            //All Records
            for (int i = 0; i < dgvItems.Rows.Count; i++)
            {
                count = 0;
                for (int c = 0; c < dgvItems.Columns.Count; c++)
                {
                    if (tempFile == true)
                    {
                        switch (dgvItems.Columns[c].Name)
                        {
                            case "Weight":
                            case "Quantity":
                            case "GrossCost":
                            case "RateExpected":
                            case "OtherCharges":
                            case "WeightCalculate":
                            case "Process":
                            case "Add":
                            case "Delete": break;
                            case "ProcessCost":
                                {
                                    exSheet.Cells[lastRow + 1, count + 1] = Convert.ToDecimal(dgvItems["GrossCost", i].Value) + Convert.ToDecimal(dgvItems["OtherCharges", i].Value);
                                    Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                                    formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                                    count += 1;
                                    break;
                                }
                            case "ItemID":
                                {
                                    exSheet.Cells[lastRow + 1, count + 1] = Convert.ToInt16(dgvItems[c, i].Value) + 1;
                                    Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                                    formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                                    count += 1;
                                    break;
                                }
                            case "PartCode":
                                {
                                    exSheet.Cells[lastRow + 1, count + 1] = "'" + dgvItems[c, i].Value;
                                    Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                                    formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                                    count += 1;
                                    break;
                                }
                            case "Size":
                                {
                                    string strText=string.Empty;
                                    if (!string.IsNullOrEmpty(Convert.ToString(dgvItems[c, i].Value)))
                                    { strText = Convert.ToString(dgvItems[c, i].Value).Replace(',', '*').Substring(0, Convert.ToString(dgvItems[c, i].Value).Length - 1); }

                                    exSheet.Cells[lastRow + 1, count + 1] = strText;
                                    Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                                    formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                                    count += 1;
                                    break;
                                }
                            default:
                                {
                                    exSheet.Cells[lastRow + 1, count + 1] =  dgvItems[c, i].Value;
                                    Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                                    formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                                    count += 1;
                                    break;
                                }
                        }
                    }                    
                    else
                    {
                        if (dgvItems.Columns[c].Name == "PartCode") { exSheet.Cells[lastRow + 1, count + 1] = "'" + dgvItems[c, i].Value; }
                        else                                        { exSheet.Cells[lastRow + 1, count + 1] = dgvItems[c, i].Value; }                            
                            Excel.Range formatRange = (Excel.Range)exSheet.Cells[lastRow + 1, count + 1];
                            formatRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, misValue);
                            count += 1;
                       
                    }
                }
                lastRow += 1;
            }
            exSheet.Columns.AutoFit();            
        }

        private void CopyExistingQuote()
        {
            string path = txtQuoteNo.Text.Replace(@"/", "");
            path = Path.GetFullPath(@"Quote List\" + path + ".xlsx");
            string query = "Select ItemID,[Part Code] as PartCode,[Part Name] as PartName,Qty as [Quantity]," +
                           "[Raw Material] as RawMaterial,Rate,[Size],Weight,Cost," +
                           "[Process Cost] as ProcessCost,[Gross Cost] as GrossCost,[Other Charges] " +
                           " as OtherCharges,Profit,[Final Cost] as FinalCost,[Rate Expected]" +
                           " as RateExpected " +
                           " from [Sheet1$]";
            DataTable dt = DataAccessManager.FetchData(path, query);

            dgvItems.Rows.Clear();
            itemID = -1;
            //int noOfItems;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dgvItems.Rows.Add();
                dgvItems["ItemID", i].Value = dt.Rows[i]["ItemID"].ToString();
                dgvItems["PartCode", i].Value = dt.Rows[i]["PartCode"].ToString();
                dgvItems["PartName", i].Value = dt.Rows[i]["PartName"].ToString();
                dgvItems["Quantity", i].Value = dt.Rows[i]["Quantity"].ToString();
                dgvItems["RawMaterial", i].Value = dt.Rows[i]["RawMaterial"].ToString();
                dgvItems["Rate", i].Value = dt.Rows[i]["Rate"].ToString();
                dgvItems["Size", i].Value = dt.Rows[i]["Size"].ToString();
                dgvItems["Weight", i].Value = dt.Rows[i]["Weight"].ToString();
                dgvItems["Cost", i].Value = dt.Rows[i]["Cost"].ToString();
                dgvItems["ProcessCost", i].Value = dt.Rows[i]["ProcessCost"].ToString();
                dgvItems["GrossCost", i].Value = dt.Rows[i]["GrossCost"].ToString();
                dgvItems["OtherCharges", i].Value = dt.Rows[i]["OtherCharges"].ToString();
                dgvItems["Profit", i].Value = dt.Rows[i]["Profit"].ToString();
                dgvItems["FinalCost", i].Value = dt.Rows[i]["FinalCost"].ToString();
                dgvItems["RateExpected", i].Value = dt.Rows[i]["RateExpected"].ToString();
                //  noOfItems += 1;
            }

            //Fill ltItems here
            ltItems.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ItemProcessEntity item = new ItemProcessEntity();
                item.ItemID = Convert.ToInt32(dt.Rows[i]["ItemID"]);

                query = "Select ItemID,[Select],ProcessName,Value,Unit,Rate,Cost from [Sheet2$] where ItemID=" + item.ItemID;
                DataTable dtProcess = DataAccessManager.FetchData(path, query);

                List<ProcessEntity> processList = new List<ProcessEntity>();
                for (int c = 0; c < dtProcess.Rows.Count; c++)
                {
                    ProcessEntity process = new ProcessEntity();
                    process.Select = Convert.ToBoolean(dtProcess.Rows[c]["Select"]);
                    process.ProcessName = Convert.ToString(dtProcess.Rows[c]["ProcessName"]);
                    process.Value = Convert.ToDecimal(dtProcess.Rows[c]["Value"]);
                    process.Unit = Convert.ToString(dtProcess.Rows[c]["Unit"]);
                    process.Rate = Convert.ToDecimal(dtProcess.Rows[c]["Rate"]);
                    process.Cost = Convert.ToDecimal(dtProcess.Rows[c]["Cost"]);
                    processList.Add(process);
                }
                item.processList = processList;
                ltItems.Add(item);
            }
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(cmbCustomer.Text))
            { MessageBox.Show("Please select customer first"); return false; }

            
            if (string.IsNullOrEmpty(cmbContact.Text))
            {MessageBox.Show("Please select Primary Contact first"); return false;}

            if (string.IsNullOrEmpty(txtQuoteNo.Text.Trim()))
            { MessageBox.Show("Quote No cannot be empty"); return false; }

            return true;
        }
        
        #endregion        

       
    }
}
