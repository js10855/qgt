﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace QGT
{
    public partial class frmCustomer : Form
    {
        string path = Path.GetFullPath(@"Master Data\Customer.xlsx");
        public frmCustomer()
        {
            InitializeComponent();
            FillGrid();
            SetGridFirstTime();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {           
            txtCustomerName.Text = "";
            txtRegCode.Text = "";
            txtAddress.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomerName.Text.Trim()) || string.IsNullOrEmpty(txtRegCode.Text.Trim()))
            {
                MessageBox.Show("Both Customer Name and Registration code is mandatory");
            }
            else
            { 
                int custID=0;
                string query = "Select max(CustomerID) as customerID from [sheet1$] ";
                DataTable dt = DataAccessManager.FetchData(path, query);
                if (!string.IsNullOrEmpty(dt.Rows[0]["CustomerID"].ToString()))
                {
                    custID= Convert.ToInt32(dt.Rows[0]["CustomerID"]) +1;
                    }
                else
                {
                    custID +=1;
                }

                query = "Insert into [sheet1$] " +
                    "(CustomerID,CustomerName,RegistrationCode,Address) values( " +
                    custID + ",'" + txtCustomerName.Text + "','" + txtRegCode.Text +  "','" + txtAddress.Text + "')";

                DataAccessManager.UpdateData(path, query);

                MessageBox.Show("Customer Record added successfully");
                FillGrid();
            }
        }

        private void FillGrid()
        {
            string query = "Select CustomerID,CustomerName as [Customer Name],RegistrationCode as [Reg Code],Address  from [sheet1$]  ";
            DataTable dt = DataAccessManager.FetchData(path, query);
            dgvCustomer.DataSource = dt;

            dgvCustomer.Columns["CustomerID"].Visible = false;         
        }

        private void SetGridFirstTime()
        {
            DataGridViewImageColumn save = new DataGridViewImageColumn();
            save.HeaderText = "Save";
            save.Name = "Save";
            save.Image = Properties.Resources.Save;
            dgvCustomer.Columns.Add(save);

            DataGridViewButtonColumn contact = new DataGridViewButtonColumn();
            contact.HeaderText = "Add Contact";
            contact.Name = "ManageContact";
            contact.UseColumnTextForButtonValue = true;
            contact.Text = "Manage";            
            dgvCustomer.Columns.Add(contact);
        }

        private void dgvCustomer_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            if (dgvCustomer.Columns[e.ColumnIndex].Name == "Save") 
                dgvCustomer.Cursor = Cursors.Hand;
            else
                dgvCustomer.Cursor = Cursors.Default;
        }

        private void dgvCustomer_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            { return; }

            if (dgvCustomer.Columns[e.ColumnIndex].Name == "Save")
            {
                string query = "Update [Sheet1$] set CustomerName ='" + dgvCustomer["Customer Name", e.RowIndex].Value +
                    "'," + "RegistrationCode='" + dgvCustomer["Reg Code", e.RowIndex].Value +
                    "'," + "Address='" + dgvCustomer["Address", e.RowIndex].Value +                 
                    "'"  + " where customerID='" + Convert.ToInt32(dgvCustomer["CustomerID",e.RowIndex].Value) +"'";
                DataAccessManager.UpdateData(path, query);
                MessageBox.Show("Customer Record saved successfully");               
            }

            if (dgvCustomer.Columns[e.ColumnIndex].Name == "ManageContact")
            {
                frmContact frmCon = new frmContact();
                frmCon.CustomerName = dgvCustomer["Customer Name",e.RowIndex].Value.ToString();
                frmCon.CustomerID  = Convert.ToInt32(dgvCustomer["CustomerID", e.RowIndex].Value);
                frmCon.ShowDialog();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }        
    }
}
